# CySCA Resource Wiki

<img src="/img/cysca18.jpg"  width="350">

Welcome to the preparation page for [Cyber Security Challenge Australia](https://cyberchallenge.com.au) (CySCA). Here you will find general and specific resources to help you prepare for CySCA. Please do not feel overwhelmed, please reach out if needed. 

*All information is provided as is and no responsibility or liability will be taken for any negative actions.*

*This wiki is still a WIP - thanks for your patience*

## The Challenge

CySCA is a 24 hour national hacking competition. Usually, multiple independent teams of 4 participants will battle it out and solve challenges released on the day. Participants will connect to CySCA infrastructure using unique VPN packs to access challenges, scoreboards, and other resources

## Categories

There are various subjects and categories can focus on. The main categories (subject to change) include:

- Corporate pentest
- Web application pentest
- Exploitation
- Forensics
- Active Defence (real time and analysis)
- Misc

## Past Challenges

A great way to start is to start with past CySCA competitions that are available to virtualise:

* [2018](https://cyberchallenge.com.au/2018/inabox/index.html)
* [2017](https://cyberchallenge.com.au/2017/inabox/index.html)
* [2017 IoT](https://cyberchallenge.com.au/2017/iot_inabox/index.html)
* [2014](https://cyberchallenge.com.au/2014/in-a-box.html)