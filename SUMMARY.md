# Summary

* [CySCA Resource Wiki](README.md)
    * [Help, I am totally new to this](docs/advice.md)
    * [Beginner Resources](docs/gettingstarted.md)
    * [CTF](docs/ctf.md)
    * [Web](docs/web.md)
    * [Penetration Testing](docs/pentest.md)
    * [Forensics & IR](docs/forensics_ir.md)
    * [Exploit / Reverse Engineering](docs/exploit.md)
    * [Misc / Programming](docs/programming.md)
* [Setup](docs/setup.md)
* [*🔙 Back to landing page*](https://cysca.utscyber.org)
