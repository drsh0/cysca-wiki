# Web

## Start Here!

* [**Pentesterlab**](https://www.pentesterlab.com/exercises/web_for_pentester/course) an amazing resource for anyone getting started on web pentesting. The free lab comes with an [ISO](https://www.pentesterlab.com/exercises/web_for_pentester/course) that you can download to do the exercises step by step on your own machine. 

* Play around with some of the wargames below. 


## Books

* [Web App Hacker's Handbook](https://find.lib.uts.edu.au/search.do?Ntt=The+Web+Application+Hacker%27s+Handbook&N=0)
* [Portswigger Web Security Academy](https://portswigger.net/web-security)

## Wargames / Activities

* [Natas](https://overthewire.org/wargames/natas/)
* [OWASP Juice Shop](https://github.com/bkimminich/juice-shop) (deploy to Heroku for free)
* [HackThisSite](https://www.hackthissite.org/)
* [Try2Hack](http://try2hack.nl)

## Tools

* Chrome / Firefox devtools
* Understand how `curl` can be used to craft HTTP methods and messages
* [Burp Suite](https://portswigger.net/burp/communitydownload)