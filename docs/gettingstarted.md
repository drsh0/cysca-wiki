# Beginner Resources

## Lab and Online Resources

* [ImmersiveLabs Digital Labs](https://dca.immersivelabs.online/)- these labs allow you start learning the basics of cybersecurity without having to set up your own lab. Activities are in bite sized chunks and it is *free* if you sign up with your student ID

* [OverTheWire](http://overthewire.org/wargames/) - start with `Bandit` and `Natas`. If you get stuck you can ask for help or find some hints. Make notes. 

* [CTF101](https://ctf101.org/) and [IntroCTF](https://trailofbits.github.io/ctf/intro/) - This will allow you to become more acquainted with CTF mechanics

* [TryHackMe](https://tryhackme.com/)

## Media

There are various YouTube channels that offer great insight into fundamental security concepts and skills. Have a look at:

* [John Hammond](https://www.youtube.com/channel/UCVeW9qkBjo3zosnqUbG7CFw)
* [LiveOverflow](https://www.youtube.com/channel/UClcE-kVhqyiHCcjYwcpfj9w)
* [NullByte](https://www.youtube.com/channel/UCgTNupxATBfWmfehv21ym-g)
* Some other [suggestions](https://old.reddit.com/r/DataHoarder/comments/c8s2s0/youtube_educational_hacking_content_getting_banned/espyxri/)

## Others

* There are various books available to borrow from UTS Library
* Lynda access is also an option for online video courses