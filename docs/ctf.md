# CTFs

Capture the flag (CTF) competitions, new or old, are a great resource to learn more about the types of challenges that you can expect. 

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/8ev9ZX9J45A" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## CTF Calendar

You can subscribe to the calendar below to keep a track of upcoming CTFs that you can participate in. 

<iframe src="https://calendar.google.com/calendar/embed?src=ctftime%40gmail.com&ctz=Australia%2FSydney" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>

## Past CTFs

Either attempting these or studying the writeups are a fantastic study strategy. 

* PicoCTF
* CSAW CTF
* Google CTF
* Plaid CTF