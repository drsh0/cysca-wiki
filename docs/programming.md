# Programming / Misc

Misc challenges are varied and fun! This page serves as a way to get you started on learning scripting languages. Please have a look at CySCA 2018 Misc writeups ([pdf](https://cyberchallenge.com.au/pdf/CySCA2018-Miscellaneous.pdf)) for a better idea on what to expect. 

## Programming

* As mentioned earlier, a scripting language is essential for continued success. Please practice learning and refining Python3.

### Beginner
* [Automate the boring stuff / Python](https://automatetheboringstuff.com/)
* [Cybrary Python Course](https://www.cybrary.it/course/python/)
* [JS in 14 minutes](https://jgthms.com/javascript-in-14-minutes/)

### Novice

* [Violent Python](https://www.amazon.com/Violent-Python-Cookbook-Penetration-Engineers/dp/1597499579)