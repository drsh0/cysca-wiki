# What the fvck is security and how do I get started?
_Thanks [@ns0j](https://github.com/Jason-Ko) for the write up!_

## The Preface

So let's preface this... _thing_ by saying that security is definitely not the easiest thing to get into. But it is absolutely one of the most fun and rewarding fields that you can break into. Security is a journey about learning about all of the things and if you decide to pursue a career in this field, you're in for a long and bumpy ride. There are going to be times when you're learning new things or playing with challenges/CTFs and you hit a brick wall and have no idea what you're doing or can't understand what you're reading and it's going to absolutely suck. But persistance (ha, inside joke) and resilience is the name of the game, don't get caught up on your struggles and don't be afraid to ask for help because we're all in this together. What we're trying to say is don't give up if you're stuck, it's a long journey and along the way you just might meet some amazing people and do some spectacular things.

## So where the fvck do you start?

There are some absolutes that you'll need to know if you want to progress. The foundations of hackyhackhacks if you will. Let's go through some of the concepts or technology that you will need to know.

### 1. The Linux Environment

First and foremost is getting familiar with the linux environment and the command line. You will be encountering and using a linux environment (Kali Linux) most of the time and knowing how to perform operations and navigate around the system is vital. Some quick examples of things that you might need to do or know are:

* Moving exploit files around the system from one folder to another folder
* Using a command line text editor to quickly modify files on remote systems
* Navigate around log files and read them
* Knowledge of where a PHP web server is started and how to serve files
* Commecting to a VPN or FTP server through the command line
* Searching for particular files or strings in a huge system with thousands of files and folders

There are some good resources out there that teach you about the linux command line, the one that we recommend for most beginners to learn about the command line interface is the wargames over at [OverTheWire](http://overthewire.org/wargames/). OverTheWire also has a host of different wargames that you can use to practice your hackyhackhack skills, but we'll go into that a bit later.

Another resource for those that like to learn by reading would be "The Linux Command Line" by William Shotts. You can download the book free [here](http://linuxcommand.org/tlcl.php), it's a great resource that goes quite in depth, even going through some bash scripting that you mind find useful. Props to William Shotts by producing such an amazing book!

[Ryans Tutorials](https://ryanstutorials.net/linuxtutorial/) is a resource developed by an ex academic of UTS, Ryan Chadwick who has put up some brilliant tutorials on various subjects including bash scripting and HTML/CSS. All credit goes to him for providing these for free use!