# Forensics and Incident Response

## Forensics

* Tools that will need to be learnt include: Volatility, Wireshark, Disassemblers like Ghidra, IDA, Binaryninja. <TODO>

## Incident Response

* [Splunk Fundamentals](https://www.splunk.com/en_us/training/courses/splunk-fundamentals-1.html) is an essential (free) course to take on. There is however no guarantee that Splunk will be appear in subsequent CySCA challenges (2018).  