# Setup

## Hypervisors

Learning in a virtualised environment is a great way to practice and become familiar with the tools and workflow. Choose your favourite hyperisor (we recommend VMWare Workstation) and start practicing! 

* VMWare Workstation - [free](https://www.uts.edu.au/current-students/current-students-information-faculty-engineering-and-it/software-download) for UTS Students

    * Alternatively, VirtualBox, QEMU, KVM, or any other hypervisors of your choice.

* If running Windows, [WSL](https://docs.microsoft.com/en-us/windows/wsl/install-win10) is highly recommended to be set up so you can get started straight away. 

## Operating Systems

You may start by virtualising a penetration testing focused linux distro. Aim to become acquainted with the tools provided in the distros.

* [Kali Linux](https://www.offensive-security.com/kali-linux-vm-vmware-virtualbox-image-download/)

* [Parrot OS](https://www.parrotsec.org/download-security.php) (choose `virtual appliance`)

## Networking

* If virtualising, it is best to keep the network adapter in NAT mode as this prevents the guest VM such as Kali from being able to access your host home or work network. 

* **NOTE** - If you are practicing and testing with vulnerable VMs from places like [Vulnhub](https://www.vulnhub.com) then please ensure that both the guest attacker OS (such as Kali) and the guest victim OS (from Vulnhub) are a part of the same NAT network. With VMWare workstation and Virtualbox, a new NAT network can be created from the app menus.  